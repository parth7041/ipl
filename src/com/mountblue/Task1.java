package com.mountblue;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Task1 {


    void findTotalMatchesByYear() throws FileNotFoundException {
        Database d=new Database();
        Map<Integer, ArrayList> map =d.database();
        Map<String,Integer> totalMatches =new TreeMap<>();
        for(Map.Entry<Integer,ArrayList> v:map.entrySet())
        {

            if(totalMatches.containsKey(map.get(v.getKey()).get(1))==false)
            {
                totalMatches.put(map.get(v.getKey()).get(1).toString(),1);
            }
            else
            {
                totalMatches.put(map.get(v.getKey()).get(1).toString(),totalMatches.get(map.get(v.getKey()).get(1))+1);
            }

        }
        System.out.println(totalMatches);
    }





    void findTotalMatchesWonByTeam() throws FileNotFoundException {
        Database d=new Database();
        Map<Integer, ArrayList> map =d.database();
        Map<String,Integer> totalWinByTeam =new TreeMap<>();
        for(Map.Entry<Integer,ArrayList> v:map.entrySet())
        {
                if (totalWinByTeam.containsKey(map.get(v.getKey()).get(10)) == false) {
                    totalWinByTeam.put(map.get(v.getKey()).get(10).toString(), 1);
                } else {
                    totalWinByTeam.put(map.get(v.getKey()).get(10).toString(), totalWinByTeam.get(map.get(v.getKey()).get(10)) + 1);
                }
        }
        totalWinByTeam.remove("");
        System.out.println(totalWinByTeam);
    }



    void findExtraRunByTeamByYear() throws FileNotFoundException {
        Database d=new Database();
        Map<Integer,ArrayList> matches=d.database();
        Map<Integer,ArrayList> deliveries=d.database2();
        String s="2016";
        ArrayList id=new ArrayList<>();
        Map<String,Integer> totalExtraRuns=new LinkedHashMap<String,Integer>();

        for(Map.Entry<Integer,ArrayList> m:matches.entrySet())
        {
            if(s.equals(matches.get(m.getKey()).get(1).toString()))
            {
                id.add(matches.get(m.getKey()).get(0));
            }
        }

            for (Map.Entry<Integer, ArrayList> m : deliveries.entrySet())
            {
                if(id.contains(deliveries.get(m.getKey()).get(0)))
                {
                    if(totalExtraRuns.containsKey(deliveries.get(m.getKey()).get(3).toString())==false)
                    {
                        int sum1 = Integer.parseInt(deliveries.get(m.getKey()).get(16).toString());
                        totalExtraRuns.put(deliveries.get(m.getKey()).get(3).toString(),sum1);
                    }
                    else
                    {
                        int sum2 = Integer.parseInt(totalExtraRuns.get(deliveries.get(m.getKey()).get(3)).toString())+
                                Integer.parseInt(deliveries.get(m.getKey()).get(16).toString());

                        totalExtraRuns.put(deliveries.get(m.getKey()).get(3).toString(), sum2);
                    }
                }
            }
        System.out.println(totalExtraRuns);
    }




    void findMinEconomicalBowler() throws FileNotFoundException {
        Database d=new Database();
        Map<Integer,ArrayList> matches=d.database();
        Map<Integer,ArrayList> deliveries=d.database2();
        String s="2015";
        ArrayList id=new ArrayList<>();
        Map<String,Integer> totalExtraRuns=new LinkedHashMap<String,Integer>();

        for(Map.Entry<Integer,ArrayList> m:matches.entrySet())
        {
            if(s.equals(matches.get(m.getKey()).get(1).toString()))
            {
                id.add(matches.get(m.getKey()).get(0));
            }
        }
        Map<String,Integer> bowler=new LinkedHashMap<>();
        Map<String,Integer> runs=new LinkedHashMap<>();
        String str1="";
        String str2="";
        int count=0;
        for(Map.Entry<Integer,ArrayList> m:deliveries.entrySet())
        {
            if(id.contains(deliveries.get(m.getKey()).get(0)))
            {

                str2=deliveries.get(m.getKey()).get(8).toString();
                if(str1.equals(str2)==false)
                {
                    str1=str2;
                    if(bowler.containsKey(str2)==false)
                    {
                        bowler.put(str2,1);
                    }
                    else
                    {
                        bowler.put(str2,bowler.get(str2)+1);
                    }
                }


                if(runs.containsKey(str2)==false)
                {
                    int sum1=Integer.parseInt(deliveries.get(m.getKey()).get(17).toString());
                    runs.put(str2,sum1);
                }
                else
                {
                    int sum2=Integer.parseInt(runs.get(str2).toString())+Integer.parseInt(deliveries.get(m.getKey()).get(17).toString());
                    runs.put(str2,sum2);
                }
            }
        }
        Map<String,Float> averageOfBowler=new LinkedHashMap<>();
        for(Map.Entry<String,Integer> m:bowler.entrySet())
        {
            if(averageOfBowler.containsKey(m.getKey())==false)
            {
                float average=(float)runs.get(m.getKey())/bowler.get(m.getKey());
                averageOfBowler.put(m.getKey(),average);
            }
        }
        float average=Float.MAX_VALUE;
        String nameOfBowler="";
        for(Map.Entry<String,Float> m:averageOfBowler.entrySet())
        {
                if(average>m.getValue())
                {
                    average=m.getValue();
                    nameOfBowler=m.getKey();
                }
        }
        System.out.println(nameOfBowler+" : "+average);
    }
}


