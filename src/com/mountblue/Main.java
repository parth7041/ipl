package com.mountblue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        Task1 t=new Task1();
        t.findTotalMatchesByYear();
        t.findTotalMatchesWonByTeam();
        t.findExtraRunByTeamByYear();
        t.findMinEconomicalBowler();
    }
}
