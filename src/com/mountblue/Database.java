package com.mountblue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Database  {

    public Map<Integer, ArrayList> database() throws FileNotFoundException {

        File file=new File("matches.csv");
        Scanner scan=new Scanner(file);
        Map<Integer, ArrayList> database=new LinkedHashMap<>();
        int i=1;
        scan.nextLine();
        while (scan.hasNext()) {
            String data = scan.nextLine();
            String value[] = data.split(",");
            ArrayList a=new ArrayList(Arrays.asList(value));
            database.put(i, a);
            i++;

        }
       return database;
    }


    public Map<Integer, ArrayList> database2() throws FileNotFoundException {

        File file=new File("deliveries.csv");
        Scanner scan=new Scanner(file);
        Map<Integer, ArrayList> database=new LinkedHashMap<>();
        int i=1;
        scan.nextLine();
        while (scan.hasNext()) {
            String data = scan.nextLine();
            String value[] = data.split(",");
            ArrayList a=new ArrayList(Arrays.asList(value));
            database.put(i, a);
            i++;

        }
        return database;
    }
}
